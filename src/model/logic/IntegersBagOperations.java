package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {

	
	
	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	
	/**
	 * Retorna la cantidad de numeros primos que hay en la bolsa
	 * @param bag
	 * @return cantidad de numeros primos
	 */
	public int cPrimos(IntegersBag bag){
		int cant = 0;
		int act;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				act = iter.next();
				Boolean si = false;
				for(int i = 1; i<(act+1) && !si; i++){
					if(act%i!=0){
						cant++;
						si = true;
					}
				}
			}
		}
		return cant;
	}
	
	public int getMin(IntegersBag bag){
		int min = Integer.MAX_VALUE;
		int act;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				act = iter.next();
				if(act < min){
					min = act;
				}
			}
		}
		return min;
	}
	
	public int cPares(IntegersBag bag){
		int cant = 0;
		int act;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				act = iter.next();
				if(act%2 == 0){
					cant++;
				}
			}
		}
		return cant;
	}
}
